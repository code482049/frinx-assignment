import json
import psycopg2

class DatabaseConnect:
    
    # Constructor handles the credentials, connection and cursor creation
    def __init__(self):
        
        # Database credentials
        self.connect = psycopg2.connect(   
            host="localhost",
            database="postgres",
            user="postgres",
            password="password",
            port= '5432')
        
        # Setting up autocommit and cursor connection
        self.connect.autocommit = True
        self.cursor = self.connect.cursor()
        
        # Fetch PostgreSQL version
        print('Connection established\nPostgreSQL database version:')
        self.cursor.execute('SELECT version()')
        db_version = self.cursor.fetchone()
        print(db_version,"\n")
        
    # Closing the connection and cursor after script finishes
    def close_connection(self) -> None:
        print("Closing the connection...")
        self.cursor.close()
        self.connect.close()

# Class that extracts data from JSON
class ExtractData:
    def __init__(self):
        
        # Indentation names for easier access
        self.configuration = 'frinx-uniconfig-topology:configuration'
        self.native = 'Cisco-IOS-XE-native:native'
        self.interface = 'interface'
    
    # Extract JSON file
    def getJSONFile(self):
        print("Extracting JSON file...")
        with open("configClear_v2.json", encoding="UTF-8") as JSON_file:
            JSON_data = json.load(JSON_file)
            
        print("JSON file successfully extracted\n")
        
        return JSON_data
    
    # Extract JSON values
    def extractJSONData(self, JSON_file, index, group):
        
        # Getting the straightforward values
        config = JSON_file[self.configuration][self.native][self.interface][group][index]
        name = config['name']
        
        # Searching for description and max frame size
        description = self.getOptionalVals(config, index, group, 'description')
        max_frame_size = self.getOptionalVals(config, index, group, 'mtu')
        
        # Checking for and getting the port channel ID
        port_channel_id = self.getPortChannelID(config, name, JSON_file)
        
        # Stringify config to allow insertion
        config = json.dumps(config)
        
        # Build and return query with extracted data
        return self.buildQuery(group, name, description, max_frame_size, config, port_channel_id)
    
    # Extract optional JSON values
    def getOptionalVals(self, config, index, group, data):
        
        print("\tGetting the", data, "from the JSON config...")
        
        # Check if JSON value is NULL
        if data in config:
            value = config[data]
        else:
            value = "NULL"
            
        print("\t\tExtracted value:", value)
        return value
    
    # Extract port Channel ID
    def getPortChannelID(self, config, name, JSON_file):
        
        port_channel_id = 0
        
        # Check if port channel ID is applicable to the chunk
        print("\tLooking for port_channel_ID...")
        if 'Cisco-IOS-XE-ethernet:channel-group' in config:
            
            # Check for corresponding port channel name
            for x in range(2):
                port_channel_ref = config['Cisco-IOS-XE-ethernet:channel-group']['number']
                port_channel_name = JSON_file[self.configuration][self.native][self.interface]['Port-channel'][x]['name']
                
                # Return corresponding port channel name if applicable
                if port_channel_ref == port_channel_name:
                    print("\t\tPort_channel name", port_channel_name, "matches the reference")
                    port_channel_id = JSON_file[self.configuration][self.native][self.interface]['Port-channel'][x]['Cisco-IOS-XE-ethernet:service']['instance'][0]['id']
                    print("\t\tPort_channel_ID found:", port_channel_id)
        
        # If no port channel ID is present, return NULL
        else:
            print("\t\tNo available port_channel_ID")
            port_channel_id = 'NULL'
        
        return port_channel_id
     
    # Build query to be executed
    def buildQuery(self, group, name, description, max_frame_size, config, port_channel_id):
        
        query = """
                INSERT INTO interface_configuration (
                name, 
                description, 
                max_frame_size,
                config,
                port_channel_id
                )
                VALUES ('{0}{1}', '{2}', {3}, '{4}', {5})
                """.format(group, name, description, max_frame_size, config, port_channel_id)
        
        print("\nRelevant JSON data successfully extracted")
        return query

class InitializeTable(DatabaseConnect):
    def __init__(self):
        super().__init__()
    
    # Initialises the table regardless of whether it already exists
    def initializeTable(self):
        
        # Creating table if it doesn't exist...
        query = """
                CREATE TABLE IF NOT EXISTS interface_configuration (
                id serial PRIMARY KEY,
                connection INT,
                name VARCHAR(255) NOT NULL,
                description VARCHAR(255),
                config JSON,
                type VARCHAR(50),
                infra_type VARCHAR(50),
                port_channel_id INT,
                max_frame_size INT
                )
                """
        self.cursor.execute(query)
        print("Table 'interface_configuration' has been successfully created or already exists")
        
        # Clearing the table to accommodate updated data
        query = "TRUNCATE TABLE interface_configuration"
        self.cursor.execute(query)
        print("Table 'interface_configuration' has been successfully truncated to accommodate updated data")

# Execute query passed to the function
class ExecuteQuery(DatabaseConnect):
     def __init__(self):
         super().__init__()
         
     def executeQuery(self, query):
         self.cursor.execute(query)
         print("New data has been successfully added to the database\n")
        
def main():
    # Object initialisation
    extract_data = ExtractData()
    initialize_table = InitializeTable()
    executeQuery = ExecuteQuery()
    
    try:
        initialize_table.initializeTable()
        JSON_file = extract_data.getJSONFile()
        
        # Extract and insert Port_channel data
        for x in range(2):
            query = extract_data.extractJSONData(JSON_file, x, 'Port-channel')
            executeQuery.executeQuery(query)
        
        # Extract and insert TenGigabitEthernet data
        for x in range(4):
            query = extract_data.extractJSONData(JSON_file, x, 'TenGigabitEthernet')
            executeQuery.executeQuery(query)
        
        # Extract and insert GigabitEthernet data
        for x in range(3):
            query = extract_data.extractJSONData(JSON_file, x, 'GigabitEthernet')
            executeQuery.executeQuery(query)

    except (Exception, psycopg2.DatabaseError) as Error:
        print(Error)
    
    finally:
        initialize_table.close_connection()
    
if __name__ == '__main__':
    main()
